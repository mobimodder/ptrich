Platinumpay
==========

Based on source code of openblock.com, A universal wallet that accept different encryptcoins, such as Bitcoin, Platinumcoin,Litecoin

LICENSE
==========
PlatinumPay is released under the LICENSE of AGPL v3
http://www.gnu.org/licenses/agpl-3.0.txt


Install
==========

checkout this repository and run
```
npm install
cp config.template.json config.json
# Edit config.json to change some settings
```

Run
==========

```
node start.js -p 10000
```